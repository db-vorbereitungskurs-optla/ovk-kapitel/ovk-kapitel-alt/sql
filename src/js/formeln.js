var $ = require("jquery");

$(document).ready(function(){

  function typeset(code) {
    MathJax.startup.promise = MathJax.startup.promise
      .then(() => MathJax.typesetPromise(code()))
      .catch((err) => console.log('Typeset failed: ' + err.message));
    return MathJax.startup.promise;
  }

  $('#dynamic_typeset_button').click(function () {
    // return array of elements to typeset or null to typeset the whole page
    typeset(() => {
      element = $('#dynamic_typeset');
      element.html('\\( a_1^b \\)');
      return element;
    });
  });
});
