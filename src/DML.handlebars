<p>
    {{> theorie }}Datenanweisungen dienen dazu, die zuvor mit Schemaanweisungen angelegten
    Datenbanktabellen mit Daten zu füllen, Auszüge aus dem Datenbestand auszugeben, Daten zu verändern
    oder zu löschen. In diesem Kapitel wollen wir die Möglichkeiten kennenlernen, mithilfe der
    Abfragesprache <span class="bold">SQL</span> Datenanweisungen zu formulieren.
</p>
<p>Doch bevor es richtig losgeht, ein paar Hinweise zu SQL:</p>
<div class="alert ovk-story" role="box">
    <ul class="ovk-bullet-list">
        <li>Es gibt verschiedene <b>SQL-Dialekte</b>, d.h. die SQL-Anweisungen können sich von Datenbank
            zu Datenbank unterscheiden. Die hier aufgeführte Anweisungen gelten für PostgreSQL v12, die
            Datenbank, die wir in dem {{> sqlfiddle }} bereits verwenden.
        </li>
        <li>SQL-Schlüsselwörter wie <code>SELECT</code>,&nbsp;&nbsp;<code>WHERE</code> oder
            <code>ORDER BY</code> werden wir im Folgenden groß schreiben.
            So heben sie sich besser von Tabellen- oder Spaltennamen ab. Es ist aber genauso möglich,
            diese Befehle klein zu schreiben
            (<code>select</code>,&nbsp;&nbsp;<code>where</code>,&nbsp;&nbsp;<code>order by</code>).
        <li>Zeichenketten werden in SQL nur mit einem einfachen Anführungszeichen <code>'</code>
            umschlossen: <code>'Schulz'</code>. Anders als bei den strings in Python können hier
            doppelte Anführungszeichen nicht verwendet werden.</li>
    </ul>
</div>


<h2>Datenbankabfragen</h2>
<p>Grundlage einer jeden Datenbankabfrage bildet der Befehl <code>SELECT</code>, mit dem angegeben wird,
    welche Tabellenspalten für die Ausgabe von Interesse sind. Sollten in dieser Hinsicht keine
    Einschränkungen erforderlich sein, kann stattdessen auch <code>*</code> verwendet werden.</p>
<ul class="ovk-list">
    <li>Die Syntax einer Datenabfrage (mit einer Bedingung versehen) sieht im Allgemeinen so aus:
        <pre class="db-code"><code class="sql hljs">
SELECT Spalte1, Spalte2, ...
FROM Tabelle
WHERE Bedingung
</code></pre>
    </li>
</ul>
<p>
    Ähnlich zu der betrachteten Übungsaufgabe im Kapitel Normalisierung soll auch hier eine Tabelle
    zur Verwaltung der Schülerdaten existieren. Diese Tabelle trägt die Bezeichnung Schüler und
    verwaltet zu jedem Schüler die Attribute Nachname, Vorname, Klassenstufe und S_Nr (als
    Primärschlüssel). Nachname und Vorname eines Schülers werden als Zeichenkette abgelegt,
    Klassenstufe und S_Nr als Zahlen.
</p>
<p>
    Auf diese Tabelle sollen nun im folgenden einige SQL-Abfragen gestellt werden.
</p>
<div id="accordion" class="ovk-accordion">
    <div class="card">
        <div class="card-header" id="headingOne">
            <h3 class="mb-0">
                <button class="btn ovk-expand" data-bs-toggle="collapse" data-bs-target="#collapseOne"
                    aria-expanded="true" aria-controls="collapseOne">
                    SELECT
                </button>
            </h3>
        </div>
    </div>
    <div class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="card-body"
            style="border-left: #efefef solid 0.15em; border-right: #efefef solid 0.15em;">
            <div>
                Zwei Beispiele zu <code>SELECT</code> soll zum besseren Verständnis beitragen.
                <ul class="ovk-list">
                    <li> Um sich einen Überblick zu verschaffen, sollen zu allen Schülern deren
                        Daten ausgegeben werden:
                        <pre class="db-code">
<code class="sql hljs">
SELECT * FROM Schueler;
</code>
</pre>
                    </li>
                    <li> Nur Vorname und Nachname aller Schüler ausgeben:
                        <pre class="db-code">
<code class="sql hljs">
SELECT Vorname, Nachname FROM Schueler;
</code>
</pre>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingTwo">
            <h3 class="mb-0">
                <button class="btn ovk-expand collapsed" data-bs-toggle="collapse"
                    data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    WHERE
                </button>
            </h3>
        </div>
    </div>
    <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordion">
        <div class="card-body"
            style="border-left: #efefef solid 0.15em; border-right: #efefef solid 0.15em;">
            <p>
                Ein häufig verwendeter Zusatz neben <code>SELECT</code> ist <code>WHERE</code>, das
                als Filter fungiert, indem in einer SQL-Abfrage nur Daten zurückgegeben werden, deren
                Werte der Bedingung entsprechen. Je nach Umfang einer Tabelle wird <code>WHERE</code>
                häufiger verwendet, da in den meisten Fällen nur bestimmte Daten von Interesse sind.
            </p>
            <ul class="ovk-list">
                <li> Alle Daten für die Schüler mit dem Vornamen 'David' ausgeben:
                    <pre class="db-code">
<code class="sql hljs">
SELECT * FROM Schueler
WHERE Vorname = 'David';
</code>
</pre>
                </li>
                <li> Vorname und Nachname der Schüler der Klassen 7 bis 10 ausgeben:
                    <pre class="db-code">
<code class="sql hljs">
SELECT Vorname, Nachname FROM Schueler
WHERE Klassenstufe >= 7 AND Klassenstufe &lt;= 10;
</code>
</pre>
                </li>
            </ul>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingThree">
            <h3 class="mb-0">
                <button class="btn ovk-expand collapsed" data-bs-toggle="collapse"
                    data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                    ORDER BY
                </button>
            </h3>
        </div>
    </div>
    <div class="collapse" id="collapseThree" aria-labelledby="headingThree" data-parent="#accordion">
        <div class="card-body"
            style="border-left: #efefef solid 0.15em; border-right: #efefef solid 0.15em;">
            <p>
                Falls die ausgegebenen Daten einer bestimmten Sortierung folgen sollen, kann mit
                Hilfe von <code>ORDER BY</code> die Bezeichnung des Attributs (Spaltenname) angegeben
                werden, nach dem die Ausgabedaten geordnet werden. Die Ordnung folgt grundsätzlich der
                aufsteigenden Sortierung. Für die absteigende Sortierung muss lediglich der Zusatz
                <code>DESC</code> mit angegeben werden. Für die aufsteigende Sortierung kann
                explizit <code>ASC</code> verwendet werden.
            </p>
            <div>
                <ul class="ovk-list">
                    <li> Alle Daten für die Schüler nach der Klassenstufe sortiert ausgeben:
                        <pre class="db-code">
<code class="sql hljs">
SELECT * FROM Schueler
ORDER BY Klassenstufe;</code>
</pre>
                    </li>
                    <li> <code>WHERE</code> und <code>ORDER BY</code> können auch kombiniert
                        eingesetzt werden, um die Schüler der Klassenstufen 7-10 absteigend sortiert
                        anzugeben:
                        <pre class="db-code">
<code class="sql hljs">
SELECT Vorname, Nachname FROM Schueler
WHERE Klassenstufe >= 7 AND Klassenstufe &lt;= 10
ORDER BY Klassenstufe DESC;
</code>
</pre>
                    </li>
                    <li>
                        Zusätzlich ist es möglich, nach mehreren Kriterien zu sortieren, wobei die
                        Reihenfolge, in der die Kriterien hinter dem <code>ORDER BY</code> eingefügt
                        werden, die Priorität der Sortierung beeinflusst. <br>
                        In diesem Beispiel werden die Schüler entsprechend der Klassenstufe absteigend
                        sortiert. Innerhalb der Klassenstufe erfolgt die Sortierung der Schüler nach dem
                        Vorname aufsteigend:
                        <pre class="db-code">
<code class="sql hljs">
SELECT * FROM Schueler
ORDER BY Klassenstufe DESC, Vorname;</code>
</pre>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header" id="headingFive">
            <h3 class="mb-0">
                <button class="btn ovk-expand collapsed" data-bs-toggle="collapse"
                    data-bs-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                    AS
                </button>
            </h3>
        </div>
    </div>
    <div class="collapse" id="collapseFive" aria-labelledby="headingFive" data-parent="#accordion">
        <div class="card-body"
            style="border-left: #efefef solid 0.15em; border-right: #efefef solid 0.15em;">
            <p>
                Der <code>AS</code> Befehl wird verwendet, um einer Tabelle oder Spalte einen
                anderen Namen zuzuweisen. Dies kann beispielsweise bei einem <code>JOIN</code>
                erforderlich werden, wenn zwei Tabellen über Spalten verfügen, die den gleichen Namen
                haben.
            </p>
            <div>
                <ul class="ovk-list">
                    <li> Benenne bei der Ausgabe der Tabelle Schüler die Spalte S_Nr in S_id um:
                        <pre class="db-code">
<code class="sql hljs">
SELECT S_Nr AS S_id, Nachname, Vorname
FROM Schueler;
</code>
</pre>
                    </li>
                </ul>
            </div>
        </div>
    </div>
