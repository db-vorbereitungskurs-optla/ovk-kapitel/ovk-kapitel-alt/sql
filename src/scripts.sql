/*  
PostgreSQL Syntax
*/

/*
MySQL Syntax
*/
CREATE TABLE Person (
    vorname varchar(255),
    nachname varchar(255),
    email varchar(255)
);

INSERT INTO Person (vorname, nachname, email)
VALUES ("Schmidt", "Elise", "eschmidt@email.de");

INSERT INTO Person (vorname, nachname, email)
VALUES ("Müller", "Arno", "arnom@online.org");

/*
MySQL Tabelle, nicht normalisiert
*/
CREATE TABLE Teilnehmer (
    vorname varchar(255),
    nachname varchar(255),
    email varchar(255)
    veranstaltung varchar(255)
    ort varchar(255)
);